<?php
/**
 * @file
 * ns_form.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function ns_form_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_node_info().
 */
function ns_form_node_info() {
  $items = array(
    'ns_form' => array(
      'name' => t('Form'),
      'base' => 'node_content',
      'description' => t('A form that can be attached to an article'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
