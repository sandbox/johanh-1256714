<?php
/**
 * @file
 * ns_demo.features.uuid_term.inc
 */

/**
 * Implementation of hook_default_terms().
 */
function ns_demo_default_terms() {
  $terms = array();

  $terms['2f097397-f180-0244-298e-73dd6a4bfc8b'] = array(
    'name' => 'Culture',
    'description' => '',
    'format' => NULL,
    'weight' => '0',
    'uuid' => '2f097397-f180-0244-298e-73dd6a4bfc8b',
    'vocabulary_machine_name' => 'ns_ch_web_topic',
    'menu' => array(
      'menu_name' => 'main-menu',
    ),
  );
  $terms['6ff6eb6a-ef3e-0c34-9914-5a8765c3c148'] = array(
    'name' => 'Business',
    'description' => '<p>Bla bla bla bla busuiness</p>',
    'format' => NULL,
    'weight' => '0',
    'uuid' => '6ff6eb6a-ef3e-0c34-9914-5a8765c3c148',
    'vocabulary_machine_name' => 'ns_ch_rss_topic',
    'field_ns_ch_rss_web_topic' => array(),
  );
  $terms['8010a053-7eda-7874-bde2-fb347b6edb2e'] = array(
    'name' => 'Culture',
    'description' => '<p>bla bla bla bla culture</p>',
    'format' => NULL,
    'weight' => '0',
    'uuid' => '8010a053-7eda-7874-bde2-fb347b6edb2e',
    'vocabulary_machine_name' => 'ns_ch_rss_topic',
    'field_ns_ch_rss_web_topic' => array(),
  );
  $terms['95491b8b-16e1-b6b4-05a8-ae9be16560e4'] = array(
    'name' => 'Business',
    'description' => '',
    'format' => NULL,
    'weight' => '0',
    'uuid' => '95491b8b-16e1-b6b4-05a8-ae9be16560e4',
    'vocabulary_machine_name' => 'ns_ch_web_topic',
    'menu' => array(
      'menu_name' => 'main-menu',
    ),
  );
  $terms['95be91b1-57fa-21f4-45ec-bb72f6b99e73'] = array(
    'name' => 'Politics',
    'description' => '<p>bla bla bla policitics</p>',
    'format' => NULL,
    'weight' => '0',
    'uuid' => '95be91b1-57fa-21f4-45ec-bb72f6b99e73',
    'vocabulary_machine_name' => 'ns_ch_rss_topic',
    'field_ns_ch_rss_web_topic' => array(),
  );
  $terms['a8f13cfa-0eb5-8b74-3187-40063557131f'] = array(
    'name' => 'Sports',
    'description' => '',
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'a8f13cfa-0eb5-8b74-3187-40063557131f',
    'vocabulary_machine_name' => 'ns_ch_web_topic',
    'menu' => array(
      'menu_name' => 'main-menu',
    ),
  );
  $terms['c53f8ba5-0277-c494-21d9-614f0666ce37'] = array(
    'name' => 'News',
    'description' => '<p>Bla bla bla bla nyheter.</p>',
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'c53f8ba5-0277-c494-21d9-614f0666ce37',
    'vocabulary_machine_name' => 'ns_ch_rss_topic',
    'field_ns_ch_rss_web_topic' => array(),
  );
  $terms['dad0d496-a88f-4304-29c5-69bfa0bc33e0'] = array(
    'name' => 'Sports',
    'description' => '<p>bla bla bla bla sports</p>',
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'dad0d496-a88f-4304-29c5-69bfa0bc33e0',
    'vocabulary_machine_name' => 'ns_ch_rss_topic',
    'field_ns_ch_rss_web_topic' => array(),
  );

  $terms['f619e200-5554-a6c4-19ad-6c86b54d8079'] = array(
    'name' => 'Politics',
    'description' => '',
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'f619e200-5554-a6c4-19ad-6c86b54d8079',
    'vocabulary_machine_name' => 'ns_ch_web_topic',
    'menu' => array(
      'menu_name' => 'main-menu',
    ),
  );
  return $terms;
}
