/**
 * This script eases the use of regions in NodeStream
 */
(function ($) {

Drupal.behaviors.adminRegion = {
  attach: function (context, settings) {
    var term = settings['ns_ch_web'].term;
    $.each(settings['ns_ch_web'].regions, function(index, region) {
      var region_settings = '<span class="region-admin ns-topic-regions-link">' + region.link + '</span>';
      $('.' + region.name).prepend(region_settings);
      $('.' + region.name + ' .region-admin').hide();
      $('.' + region.name).hover(function() {
        $('.' + region.name + ' .region-admin').show();
      }, function() {
        $('.' + region.name + ' .region-admin').hide();
      });
    });
  }
};
})(jQuery);
